package main

import (
	"fmt"
	"strings"
)

func main() {
	var palavra string
	var lista []string
	var caractere string
	contagem := 0
	fmt.Print("Dê-me uma palavra: ")
	fmt.Scan(&palavra)
	lista = make([]string, len(palavra))
	lista = strings.Split(palavra, "")
	fmt.Println(lista)
	fmt.Print("Qual caractere deseja verificar: ")
	fmt.Scan(&caractere)
	for i := 0; i < len(lista); i++ {
		if lista[i] == caractere {
			contagem = contagem + 1
		}
	}
	fmt.Println("Tal caractere apareceu", contagem, "vezes")
}
