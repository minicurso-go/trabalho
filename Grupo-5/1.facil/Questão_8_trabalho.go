package main

import "fmt"

func main() {
	var c, f float64
	fmt.Print("informe a temperatura em celcius:")
	fmt.Scan(&c)
	f = (c * 1.8) + 32
	fmt.Println(f)
}
