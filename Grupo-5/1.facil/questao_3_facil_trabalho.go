package main

import "fmt"

func main() {
	var n int
	fmt.Print("digite o número: ")
	fmt.Scan(&n)
	if n%2 == 0 {
		fmt.Print("número par")
	} else {
		fmt.Print("número ímpar")
	}
}
