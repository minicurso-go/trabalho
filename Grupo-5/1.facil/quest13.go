package main

import "fmt"

func scanT() float32 {
	var taxa float32

	fmt.Println("Qual a taxa de juros desse periodo: ")
	fmt.Scan(&taxa)

	return taxa
}
func scanP() float32 {

	var period float32

	fmt.Println("Qual o periodo em meses: ")
	fmt.Scan(&period)

	return period
}
func scanC() float32 {

	var capi float32

	fmt.Println("Qual o capital inicial: ")
	fmt.Scan(&capi)

	return capi
}

func juros() {
	taxa := scanT()
	period := scanP()
	capi := scanC()
	var mont float32
	var jurS float32

	jurS = taxa * capi * period

	mont = jurS + capi

	fmt.Println("O juros simples dos dados enviados é ", jurS, "e seu montante é ", mont)

}

func main() {
	juros()
}
