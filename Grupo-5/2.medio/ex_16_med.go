package main

import (
	"fmt"
)

func main() {
	var num1, num2 int
	var div1 []int
	var div2 []int
	var somaD1, somaD2 int
	fmt.Print("Dê-me um número: ")
	fmt.Scan(&num1)
	fmt.Print("Dê-me outro número: ")
	fmt.Scan(&num2)
	div1 = make([]int, num1)
	div2 = make([]int, num2)
	for i := 1; i <= num1; i++ {
		if num1%i == 0 {
			div1[i-1] = i
		}
	}
	div1 = append(div1[:0], div1...)
	fmt.Println(div1)
	for i := 0; i < len(div1); i++ {
		somaD1 += div1[i]
	}
	fmt.Println(somaD1)

	for i := 1; i <= num2; i++ {
		if num2%i == 0 {
			div2[i-1] = i
		}
	}
	div2 = append(div2[:0], div2...)
	fmt.Println(div2)
	for i := 0; i < len(div2); i++ {
		somaD2 += div2[i]
	}
	fmt.Println(somaD2)

	if somaD1 == somaD2 {
		fmt.Println("Os números são amigos!")
	}
}
