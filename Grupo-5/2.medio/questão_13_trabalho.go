package main

import (
	"fmt"
	"math"
)

func main() {
	var num, quad float64
	var res int
	fmt.Print("informe um valor para saber se é um quadrado perfeito:")
	fmt.Scan(&num)
	quad = math.Sqrt(num)
	res = int(quad)
	if math.Pow(float64(res), 2) == num {
		fmt.Print("o numero é um quadrado perfeito")
	} else {
		fmt.Print("o numero não é um quadrado perfeito")
	}
}
