package main

import "fmt"

func verificacao(x int) bool {
	primoBool := true
	i := 0

	if x == 1 || x == 0 {
		primoBool = false
	}

	for i = 2; i < x; i++ {
		if x%i == 0 {
			primoBool = false
		}
	}

	return primoBool
}

func main() {

	var x int

	fmt.Println("Informe um número (digite 0 para sair):")
	fmt.Scanln(&x)

	for x != 0 {
		fmt.Println("Informe um número (digite 0 para sair):")
		fmt.Scanln(&x)

	}

	fmt.Print("Verificação se sua lista é de números primos: ", verificacao(x))
}
