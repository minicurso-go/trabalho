
package main

import "fmt"

func main() {
    var x float64
    var y float64
    var z float64
    var a float64
    var b float64
    var c float64
  fmt.Println("Digite os lados do triângulo: ")
  fmt.Scan(&x, &y, &z)
  fmt.Println("Digite os ângulos internos: ")
  fmt.Scan(&a, &b, &c)
  
  if (x+y)<z || (x+z)<y || (y+z)<x || x<=0 || y<=0 || z<=0 || (a+b+c)!=180 {
      fmt.Println("O triângulo é inválido.")
  }else{
      fmt.Println("O triângulo é válido")
  }
}
