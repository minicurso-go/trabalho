package main

import "fmt"

func main() {
	var numero int

	fmt.Println("Digite um número:")
	fmt.Scan(&numero)

	if numero%numero == 0 {
		fmt.Println("O número é um quadrado perfeito")
	} else {
		fmt.Println("O número não é um quadrado perfeito")
	}

}
