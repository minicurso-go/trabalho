package main

import "fmt"

func main() {
	var num, comeco, fim float64

	fmt.Println("digite um número: ")
	fmt.Scan(&num)
	fmt.Println("começo: ")
	fmt.Scan(&comeco)
	fmt.Println("fim: ")
	fmt.Scan(&fim)

	for i := comeco; i <= fim; i++ {
		if num/i == 1 {
			fmt.Println(i)
		}
	}
}
