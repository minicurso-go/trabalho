/*
//1.Soma de dois números: Escreva um programa que recebe dois números como entrada e retorna a soma deles.
package main
import "fmt"
    
func main() {
    var x int 
    var y int 
    var soma int 
    
    fmt.Println("Digite um valor:")
    fmt.Scan(&x)
    
    fmt.Println("Digite outro valor:")
    fmt.Scan(&y)
    
    soma = x + y
    
    fmt.Println("Soma = ", soma)
}
*/


/*
//2.Média de três números: Escreva um programa que calcula a média de três números fornecidos como entrada.
package main 
import "fmt"

func main(){
    
    var n1 float32
    var n2 float32
    var n3 float32
    
    fmt.Println("Digite N1:")
    fmt.Scan(&n1)
    
    fmt.Println("Digite N2:")
    fmt.Scan(&n2)
    
    fmt.Println("Digitew N3:")
    fmt.Scan(&n3)
    
    media := (n1+n2+n3)/3
    
    fmt.Println("Media = ", media)
    
    
}
*/


/*
//3.Verificação de número par ou ímpar: Crie um programa que verifica se um número fornecido pelo usuário é par ou ímpar.
package main 
import "fmt"

func main(){

    var x int 
    
    fmt.Println("Digite um valor:")
    fmt.Scan(&x)
    
    if x %2 == 0 {
        fmt.Println("Num par")    
        }else{
        fmt.Println("Num ímpar")
        }
  */
 
 
 /* 
  //4.Cálculo de fatorial: Escreva um programa que calcula o fatorial de um número fornecido pelo usuário.
 
package main 
import "fmt"

func fatorial (x int) int {
    y := 1 
    for i:= 1 ; i <= x; i++{
        y*= i
    }
    return y
}
func main(){
    var z int
    
    fmt.Println("---Insira um valor para descobrir seu fatorial---")
    fmt.Println("Numero:")
    fmt.Scan(&z)
    fmt.Println(z, "! = ", fatorial(z))
}
*/


/*
//5.Identificação do maior de dois números: Escreva um programa que determina qual dos dois números fornecidos pelo usuário é o maior.

package main 
import "fmt"

func main(){
    
    var x float32
    var y float32
    
    fmt.Println("Digite um valor:")
    fmt.Scan(&x)
    
    fmt.Println("Digite outro valor:")
    fmt.Scan(&y)
    
    if x > y {
        fmt.Printf("%.2f é maior que %.2f", x, y)
    }else{
        fmt.Printf("%.2f é maior que %.2f", y,x)
    }
    
}
*/

/*
//6.Contagem regressiva: Escreva um programa que imprime uma contagem regressiva de 10 até 1.
package main 
import "fmt"

func main(){
    for i := 10; i > 0; i--{
        fmt.Println("Contagem regressiva",i)
    }
}
*/


/*
//7.Verificação de palíndromo: Crie um programa que verifica se uma palavra fornecida pelo usuário é um palíndromo (ou seja, se ela é a mesma se lida de trás para frente).

package main 
import (
    "fmt"
)

func main() {
    var palavra string
    
    fmt.Print("Digite uma palavra: ")
    fmt.Scan(&palavra)
    
    if
}
*/


/*8.Conversão de temperatura: Escreva um programa que converte uma temperatura fornecida em graus Celsius para Fahrenheit.
package main 
import "fmt"

func main(){
    
    var c, f float64
    
    fmt.Println("Digite temperatura em celsius:")
    fmt.Scan(&c)
    
    f = (9*c/50) + 32
    
    fmt.Printf("Valor de farenheit é %.2f: ", f)
    
}
*/

/*

9.Identificação do maior de três números: Escreva um programa que determina qual dos três números fornecidos pelo usuário é o maior.
package main 
import "fmt"

func main(){
    
    var x, y, z int
    
    fmt.Println("Digite um valor:")
    fmt.Scan(&x)
    
    fmt.Println("Digite outro valor:")
    fmt.Scan(&y)
    
    fmt.Println("Digite um valor:")
    fmt.Scan(&z)
    
    if x > y && x > z {
        fmt.Printf("%d é maior que %d e %d", x, y, z)
    }else if y > x && y > z{
        fmt.Printf("%d é maior que %d e %d", y, x, z)
    }else{
        fmt.Printf("%d é maior que %d e %d", z, y, x)
    }
    
}
*/


/*
//10.Tabuada de multiplicação: Crie um programa que imprime a tabuada de multiplicação de um número fornecido pelo usuário.
package main 
import "fmt"

func main(){
    var x int 
    
    fmt.Println("---Insira um numero para ver sua tabuada---")
    fmt.Println("Digite um valor:")
    fmt.Scan(&x)
    
    for i := 1; i <= 10; i++ {
        fmt.Println(x,"*", i, "=", x * i)
    }
    
}
*/