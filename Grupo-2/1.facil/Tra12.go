package main

import "fmt"

func main() {
	var num1 float64
	var num2 float64
	var num3 float64
	var peso1 float64
	var peso2 float64
	var peso3 float64
	var media float64

	fmt.Println("nota 1:")
	fmt.Scan(&num1)
	fmt.Println("peso da nota 1: ")
	fmt.Scan(&peso1)
	fmt.Println("nota 2: ")
	fmt.Scan(&num2)
	fmt.Println("peso da nota 2: ")
	fmt.Scan(&peso2)
	fmt.Println("nota 3: ")
	fmt.Scan(&num3)
	fmt.Println("peso da nota 3")
	fmt.Scan(&peso3)
	media = (num1 / peso1) + (num2 / peso2) + (num3 / peso3)
	fmt.Println("a média ponderada é: ", media)
}
