/*
// 16 - Contagem regressiva em intervalo personalizado: Escreva um programa que imprime uma contagem regressiva a partir de um número fornecido pelo usuário até 1.
package main

import("fmt")

func main() {
    var num int
    fmt.Print("Insira um número para fazer uma contagem regressiva a partir dele: ")
    fmt.Scan(&num)
    for i := num ; i >= 0 ; i-- {
        fmt.Println(i)
        fmt.Println("...")
    }
    fmt.Print("Fim da contagem regressiva.")
}
*/
// 17 - Geração de Números Primos: Implemente um programa que gere uma lista de números primos até um determinado limite fornecido pelo usuário.
/*
package main

import("fmt")

func primo (p int) int {
	var count int
	for i := p ; i>0 ; i-- {
		if p % i == 0 {
			count++
		}
	}
	return count
}

func main() {
    var num int
    
    fmt.Print("Insira um número para saber os números primos que existem até ele: ")
    fmt.Scan(&num)
    for i := 1 ; i <= num ; i++ {
        if primo(i) == 2 {
            fmt.Println(i)
        } 
    }
}
*/