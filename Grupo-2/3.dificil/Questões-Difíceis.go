// 8 - Cálculo da Mediana de uma Lista: Implemente um programa que calcule a mediana de uma lista de números fornecida pelo usuário.
/*
package main
    
import("fmt")
    
func main(){
    var lista []int
    var count,num int
    fmt.Println("Insira números em uma lista, digite 0 para parar de inserir")
    for i:=0 ; ; i++ {
        fmt.Print("Número ",i+1,": ")
        fmt.Scan(&num)
        if num == 0 {
            break
        } else {
         lista = append(lista,num)
         count ++
        }
    }
    if count%2 == 0 {
        fmt.Print("A mediana dos seus números ficou: " ,lista[(count/2)-1],lista[(count/2)])
    } else {
        fmt.Print("A mediana dos seus números ficou: ", lista[count/2])
    }
    for i:=0 ; i<count; i++{
    }
}
*/
// 18 - Problema do Bolsa de Valores: Implemente um algoritmo que ajuda os investidores a maximizar seus lucros na bolsa de valores.
/*
package main

import("fmt")

func main(){
    var lista []float32
    var num,max,min float32
    var count int
    
    max = 1
    min = 1000000
    
    fmt.Println("Insira valores de uma lista de bolsa de valores, digite 0 para parar de inserir")
    for i:=0 ; ; i++ {
        fmt.Print("Número ",i+1,": ")
        fmt.Scan(&num)
        if num == 0 {
            break
        } else {
         lista = append(lista,num)
         count++
        }
    }
    for i:=0 ; i<count ; i++{
        if lista[i] > max {
            max = lista[i]
        }
        if lista[i] < min {
            min = lista[i]
        }
    }
    fmt.Println("Sua lista de bolsa de valores é: ", lista)
    fmt.Print("O máximo de lucro obtido por subtrair o valor máximo pelo mínimo é: ", max-min)
}
*/
// 20 - Geração de Números de Primos Entre Dois Limites: Escreva um programa que gere todos os números primos dentro de um intervalo fornecido pelo usuário.
/*
package main

import("fmt")

func primo (p int) int {
	var count int
	for i := p ; i>0 ; i-- {
		if p % i == 0 {
			count++
		}
	}
	return count
}

func main() {
    var numInicio,numFim int
    
    fmt.Println("Insira dois números para saber os números primos que existem entre eles: ")
    fmt.Print("Inicio: ")
    fmt.Scan(&numInicio)
    fmt.Print("Fim: ")
    fmt.Scan(&numFim)
    
    if numInicio > numFim {
        fmt.Print("Intervalo errado")
    } else {
        for i := numInicio ; i <= numFim ; i++ {
            if primo(i) == 2 {
                fmt.Println(i)
            } 
        }
    }
}
*/